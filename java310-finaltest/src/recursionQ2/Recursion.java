// Guang 1942372
package recursionQ2;

public class Recursion {
	public static int recursiveCount(String[] words, int n) {
		
		if (n<words.length) {
			if (n%2==0 & words[n].contains("q") ) {
				return 1 + recursiveCount(words, n+1);
			} else {
				return recursiveCount(words, n+1);
			}
		}
		else {
//			System.out.println("Start position is out of bound in the String array provided");
			return 0;
		}
	}
}
