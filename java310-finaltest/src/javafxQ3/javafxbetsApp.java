package javafxQ3;

import java.io.IOException;
import java.util.Random;

import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;

import javafx.application.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
	
public class javafxbetsApp extends Application {
	public void start(Stage stage) {
		
		GridPane pane=new GridPane();
		Text money= new Text("Money: ");
		TextField moneyField=new TextField();
		pane.add(money, 0, 0, 1, 1);
		pane.add(moneyField, 2, 0, 4, 1);
		Text bets=new Text("Bets: ");
		TextField betsField=new TextField("");
		pane.add(bets, 0, 2);
		pane.add(betsField, 2, 2);
		
		Button evenB = new Button("Even");
		Button oddB = new Button("Odd");
		pane.add(evenB, 0, 4);
		pane.add(oddB, 4, 4);
		
		evenB.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				System.out.println("choose even");
				Random rand = new Random();
				int n = rand.nextInt(7);
				if (n%2==0) {
					System.out.println("You got it, it is even");
					moneyField.setText(Integer.toString((Integer.parseInt(moneyField.getText())+Integer.parseInt(betsField.getText()));
				} else {
					System.out.println("You lost, it is odd");
					moneyField.setText(Integer.toString((Integer.parseInt(moneyField.getText())+Integer.parseInt(betsField.getText()));
				}
				System.out.println("Current money"+moneyField.getText());
			}
		});
		
				oddB.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent e) {
							System.out.println("choose odd");
							Random rand = new Random();
							int n = rand.nextInt(7);
							if (n%2==1) {
								System.out.println("You got it, it is odd");
								moneyField.setText(Integer.toString((Integer.parseInt(moneyField.getText())+Integer.parseInt(betsField.getText()));
							} else {
								System.out.println("You lost, it is even");
								moneyField.setText(Integer.toString((Integer.parseInt(moneyField.getText())+Integer.parseInt(betsField.getText()));
							}
							System.out.println("Current money"+moneyField.getText());
						}
					});
					
		
		
		pane.setVgap(5);
		
	
	}

	public static void main(String[] args) {
		Application.launch(args);

	}

}
